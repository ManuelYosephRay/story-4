from django.urls import path
from . import views

urlpatterns = [
    path('', views.PPW3html, name = 'PPW3html'),
    path('experiences/', views.PPW3html2, name = 'PPW3html2'),
    path('education/', views.PPW3html3, name = 'PPW3html3'),
    path('contact/', views.PPW3html4, name = 'PPW3html4'),
    path('warning/', views.PPW3html5, name = 'PPW3html5')
]